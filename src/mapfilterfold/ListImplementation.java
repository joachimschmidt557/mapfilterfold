package mapfilterfold;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class ListImplementation {

	public static <T, R> List<R> map(Function<T, R> func, List<T> list) {
		
		List<R> result = new ArrayList<R>(list.size());
		for (int i = 0; i < list.size(); i++)
			result.set(i, func.apply(list.get(i)));
		return result;
		
	}
	
	public static <T> List<T> filter(Predicate<T> pred, List<T> list) {
		
		List<T> result = new ArrayList<T>(list.size());
		for (int i = 0; i < list.size(); i++)
			if (pred.test(list.get(i)))
				result.add(list.get(i));
		return result;
		
	}
	
}
